export default {
    cdn     : web ? '.' : location.origin ? `${location.origin}/game` : 'https://wisdom.lufei.so/logic',
    bgColor : 0xe0ddcc,
    mode    : 'auto',
    zoom    : {min: 1, max: 1},
    debug   : true, // 正式版务必关掉
    socket  : null,
    screen  : {
        width: 750,
        height: 1334,
        orientation: 0,
        resolution: 1,
        get ratio() {return this.width / this.height}
    },
    design  : {
        width: 750,
        height: 1334,
        mode: 'portrait',
        get ratio() {return this.width / this.height}
    }
}