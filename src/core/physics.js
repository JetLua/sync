import planck from 'planck-js'

const
    world = new planck.World(planck.Vec2(0, 0)),
    ptm = 32,
    step = 1 / ptm


function update() {
    for (let body = world.getBodyList(); body; body = body.getNext()) {
        if (body.node) {
            const
                node = body.node,
                point = body.getPosition()

            // just for this application
            node.shadow.x = point.x * ptm
            node.shadow.y = point.y * ptm
            node.shadow.rotation = body.getAngle() / Math.PI * 180
        }
    }
    world.step(step)
}



const bodyType = {
    STATIC: planck.Body.STATIC,
    DYNAMIC: planck.Body.DYNAMIC
}

export default {
    world,
    bodyType,
    update
}

PIXI.DisplayObject.prototype.addBody = function(option={}) {
    const body = world.createBody({
        type: getValue(option.type, planck.Body.DYNAMIC),
        position: planck.Vec2(this.x * step, this.y * step),
        ...option
    })

    // not good but simple
    body.node = this
    this.body = body

    return body
}

planck.Body.prototype.destroy = function() {
    this.node = null
    world.destroyBody(this)
}

planck.Body.prototype.addBox = function(option={}) {
    const
        shape = planck.Box(
            option.width * .5 * step,
            option.height * .5 * step,
            option.center ? planck.Vec2(option.center.x * step, option.center.y * step) : null,
            option.angle
        ),

        def = {
            density: getValue(option.density, 1),
            ...option
        }

    this.createFixture(shape, def)

    return this
}

planck.Body.prototype.addPolygon = function(option={}) {
    const
        shape = planck.Polygon(option.vertices.map(point =>
            planck.Vec2(point.x * step * this.node.scale, point.y * step * this.node.scale))),
        def = {
            density: getValue(option.density, 1),
            ...option
        }
    this.createFixture(shape, def)
    return this
}

planck.Body.prototype.createMouseJoint = function(point) {
    const
        ground  = world.createBody()

    return world.createJoint(
        planck.MouseJoint(
            {maxForce: 1e3},
            ground, this,
            planck.Vec2(point.x * step, point.y * step)
        )
    )
}

planck.MouseJoint.prototype.follow = function(point) {
    point.x *= step
    point.y *= step
    this.setTarget(point)
}

function getValue(v, e) {
    return v === undefined ? e : v
}