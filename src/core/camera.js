import {getValue} from '../utils'
import config from '../config'

export default class Camera extends PIXI.Container {
    target = null
    offset = {x: 0, y: 0}
    targetPoint = new PIXI.Point()

    constructor() {
        super()
    }

    setOffset(x, y) {
        y = y || 0
        this.offset.x = x
        this.offset.y = y
    }

    follow(target) {
        this.target = target
    }

    focus(t) {
        const
            dx = config.screen.width * .5 + this.offset.x,
            dy = config.screen.height * .5 + this.offset.y,
            damping = 1 - Math.exp(-t / 10)

        this.target.getGlobalPosition(this.targetPoint),
        this.targetPoint.x = dx - this.targetPoint.x
        this.targetPoint.y = (dy - this.targetPoint.y) * damping

        this.position.x += this.targetPoint.x
        this.position.y += this.targetPoint.y
    }

    update(t) {
        this.target && this.focus(t)
    }
}