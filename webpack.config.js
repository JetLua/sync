const
    path = require('path'),
    htmlWebpackPlugin = require('html-webpack-plugin'),
    webpack = require('webpack')

const isProd = process.env.NODE_ENV === 'production' ? true : false


module.exports = {
    entry: [
        './src/app.js'
    ],

    output: {
        path: path.resolve('dist'),
        filename: 'game.js'
    },

    devServer: {
        hot: true,
        contentBase: '.'
    },

    devtool: isProd ? false : 'source-map',

    module: {
        rules: [
            {
                test: /\.js$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.(vert|frag)$/,
                use: ['raw-loader']
            }
        ]
    },

    plugins: [
        new htmlWebpackPlugin({
            template: './src/template.html',
            hash: true,
            filename: 'index.html',
            inject: 'body',
            minify: {
                collapseWhitespace: true
            }
        }),

        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),

        new webpack.DefinePlugin({
            web: JSON.stringify(true),
            stamp: JSON.stringify(Date.now())
        }),

        new webpack.ProvidePlugin({
            PIXI: 'pixi.js'
        })
    ],

    mode: isProd ? 'production' : 'development'
}