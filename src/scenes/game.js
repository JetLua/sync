import core, {monitor, physics} from '../core'
import {Player} from '../components'
import config from '../config'


export default {
    players: {},

    setup() {
        this.container = new PIXI.Container()
    },

    listen() {
        config.socket
            .on('user', data => {
                for (const id in data) {
                    // console.log(id, this.players)
                    if (!this.players.hasOwnProperty(id)) {
                        const state = data[id]
                        this.players[id] = new Player(state)
                        this.container.addChild(this.players[id])
                    }
                }
            })
            .on('join', id => {
                this.players[id] = new Player()
                this.container.addChild(this.players[id])
            })
            .on('leave', id => {
                const player = this.players[id]
                player && player.destroy()
                Reflect.deleteProperty(this.players, id)
            })
            .on('sync', data => {
                this.sync(data)
            })

        monitor
            .on('keydown', vector => config.socket.emit('sync:vector', vector))
            .on('keyup', vector => config.socket.emit('sync:vector', vector))
    },

    sync(data) {
        for (const key in data) {
            const
                player = this.players[key],
                vector = data[key]

            if (!player || !vector) continue

            // player.vector.copy(vector)
            player.sync(vector)
        }

        physics.update()

        // 同步当前玩家状态
        config.socket.emit('sync:state', this.players[config.socket.id].shadow)
    },

    update() {
        for (const key in this.players) {
            const player = this.players[key]
            player && player.update()
        }
    },

    show() {
        this.setup()
        this.listen()
        core.stage.addChild(this.container)
        core.ticker.add(this.update, this)
    },

    hide() {

    }
}