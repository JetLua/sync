const
    rect = new PIXI.Rectangle(),
    fn = {
        horizontal: Symbol(0),
        vertical: Symbol(1),
        grid: Symbol(2),
        listen: Symbol(3)
    }

let delta

export default class Layout extends PIXI.Container {
    static GRID = 2
    static VERTICAL = 1
    static HORIZONTAL = 0

    constructor(option={}) {
        super()
        this.type = option.type || Layout.HORIZONTAL
        this.gap = getValue(option.gap, 10)
        this.misc = {
            x: 0,
            y: 0,
            column: getValue(option.column, 3),
            max: 0,
            index: 0
        }
        this[fn.listen]()
    }

    [fn.listen]() {
        this.on('added', () => {
            this.place()
        })
    }

    destroy() {
        super.destroy()
        this.off('added')
    }

    addChild(...args) {
        super.addChild(...args)
    }

    place() {
        this.children.forEach((child, i) => {
            switch (this.type) {
                case Layout.GRID: {
                    this[fn.grid](child, i)
                    break
                }

                case Layout.HORIZONTAL: {
                    this[fn.horizontal](child, i)
                    break
                }

                case Layout.VERTICAL: {
                    this[fn.vertical](child, i)
                    break
                }
            }
        })
        this.emit('resize', this.getLocalBounds(rect))
    }

    [fn.grid](child, i) {
        child.getLocalBounds(rect)

        const
            ox = getValue(child.ox, -rect.x / rect.width),
            oy = getValue(child.oy, -rect.y / rect.height),
            ix = this.misc.index % this.misc.column,
            iy = ~~(this.misc.index / this.misc.column)

        ix ? child.x = this.gap + this.misc.x + ox * child.width :
            child.x = ox * child.width

        this.misc.x = child.x + (1 - ox) * child.width

        iy ? child.y = this.gap + this.misc.y + oy * child.height :
            child.y = oy * child.height

        this.misc.max = Math.max(this.misc.max, child.y + (1 - oy) * child.height)

        ix === this.misc.column - 1 ? this.misc.y = this.misc.max : null

        this.misc.index++
    }

    [fn.horizontal](child, i) {
        child.getLocalBounds(rect)
        const ox = getValue(child.ox, -rect.x / rect.width)
        i ? child.x = this.gap + delta + ox * child.width : child.x = ox * child.width
        delta = child.x + (1 - ox) * child.width
    }

    [fn.vertical](child, i) {
        child.getLocalBounds(rect)
        const oy = getValue(child.ox, -rect.y / rect.height)
        i ? child.y = this.gap + delta + oy * child.height : child.y = oy * child.height
        delta = child.y + (1 - oy) * child.height
    }

}

function getValue(v, s) {
    return v === undefined ? s : v
}
