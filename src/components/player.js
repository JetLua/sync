import {physics} from '../core'
import config from '../config'

export default class Player extends PIXI.Graphics {
    velocity = new PIXI.Point()
    speed = 8
    toward = 0
    shadow = {x: 0, y: 0, rotation: 0}
    vector = new PIXI.Point()
    t = 0

    constructor(state={}) {
        super()
        this.beginFill(0xffc030)
        this.drawRect(0, 0, 50, 50)
        this.endFill()

        this.shadow.x = state.x || 0
        this.shadow.y = state.y || 0
        this.shadow.rotation = state.rotation || 0
        this.position.copy(this.shadow)

        this.addBody({
            type: physics.bodyType.DYNAMIC,
            fixedRotation: true
        }).addBox({
            width: 50,
            height: 50
        })
    }

    sync(vector) {
        // this.vector.copy(vector)
        this.velocity.x = ~~(vector.x * this.speed)
        this.velocity.y = ~~(vector.y * this.speed)
        this.body.setLinearVelocity(this.velocity)
    }

    destroy() {
        super.destroy()
        this.body.destroy()
        this.body = null
    }

    update() {
        this.x !== this.shadow.x ? this.x += ~~(this.velocity.x * .6) : null
        this.y !== this.shadow.y ? this.y += ~~(this.velocity.y * .6) : null
        // if (!this.velocity.x && !this.velocity.y) {
            this.x += ~~((this.x - this.shadow.x) * this.t * (this.t - 2))
            this.y += ~~((this.y - this.shadow.y) * this.t * (this.t - 2))

            this.t >= 1 ? this.t = 0 : this.t += .01
        // }
    }
}