import io from 'socket.io-client'
import parser from 'socket.io-msgpack-parser'
import {game, prepare} from './scenes'
import {monitor} from './core'
import config from './config'

const
    key = {
        left: 0,
        right: 0,
        up: 0,
        down: 0
    },
    vector = {x: 0, y: 0}

config.socket = io('ws://192.168.6.10:3000', {parser})
    .on('connect', () => game.show())


window.addEventListener('keydown', ev => {

    switch (ev.keyCode) {
        case 37: {
            key.left = -1
            break

        }
        case 38: {
            key.up = -1
            break

        }
        case 39: {
            key.right = 1
            break

        }
        case 40: {
            key.down = 1
            break
        }
    }

    vector.x = key.left + key.right
    vector.y = key.up + key.down

    monitor.emit('keydown', vector)
})

window.addEventListener('keyup', ev => {

    switch (ev.keyCode) {
        case 37: {
            key.left = 0
            break
        }
        case 38: {
            key.up = 0
            break
        }
        case 39: {
            key.right = 0
            break
        }
        case 40: {
            key.down = 0
            break
        }
    }

    vector.x = key.left + key.right
    vector.y = key.up + key.down
    monitor.emit('keyup', vector)
})
