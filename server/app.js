const
    parser = require('socket.io-msgpack-parser'),
    io = require('socket.io')(3000, {parser}),
    user = {}


setInterval(() => {
    io.emit('sync', filter())
}, 30)


io.on('connect', socket => {
    const info = user[socket.id] = {
        vector: {x: 0, y: 0}
    }

    socket.broadcast.emit('join', socket.id)

    socket.emit('user', filter('state'))

    socket
        .on('sync:vector', data => {
            info.vector = data
        })
        .on('sync:state', data => {
            info.state = data
        })
        .on('disconnect', () => {
            socket.broadcast.emit('leave', socket.id)
            Reflect.deleteProperty(user, socket.id)
        })
})

function filter(field='vector') {
    const data = {}
    if (field === 'vector') {
        Object.entries(user).forEach(([key, val]) => {
            data[key] = val.vector
        })
    } else if (field === 'state') {
        Object.entries(user).forEach(([key, val]) => {
            data[key] = val.state
        })
    }
    return data
}

